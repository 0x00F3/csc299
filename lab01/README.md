Name: Eleanor Holley
Student ID: 1387173
Email: eleanor.holley@gmail.com 
Kernel Version: 2.6.32-64
Machine Hardware name: 128-Ubuntu smp
Processor Type: x86_64
Operating System: Linux

Task 3:
ls is located in drwxr-xr-x
ls is 1024 bytes
ls's authors are Stallman and MacKenzie
ls will list all with the argument -a or -all

Task 4: 
/home is where the user reads and writes files
/bin contains executable files for booting.
/var contains files to which the system writes information.
/etc contains all system-related files and directories

Task 6: 
ACROSS
2
7 sudo
8 rmdir

DOWN
1 cp 
3
4 whoami
11 rm