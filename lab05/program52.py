from BeautifulSoup import BeautifulSoup as Soup
import simplejson
import re
file = open("CSC.properties.1.xml", 'r').read()
soup = Soup(file)
dtags = soup.findAll(name=re.compile('^d\:\w+'))
tagDict = {}
for tag in dtags:
    tag = str(tag)
    tag = tag[3:]
    key = ""
    value = ""
    foundKey = False
    for char in tag:
        if not foundKey:
            if char  == '>':
                foundKey = True
            else:
                key = key + char
        else:
            if char == '<':
                break
            else:
                value = value + char
    #print(key, value)
    tagDict[key] = value
simplejson.dump(tagDict, open('CSC.properties.1.json', 'w'))
