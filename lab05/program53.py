from BeautifulSoup import BeautifulSoup as Soup
import simplejson
import re
import urllib
url = "http://www.cdm.depaul.edu.edu/odata/Courses?$orderby=CatalogNbr&$filter=EffStatus%20eq%20'A'%20and%20SubjectId%20eq'CSC'"
file = urllib.urlopen(url).read()
soup = Soup(file)
mtags = soup.findAll(name=re.compile('^m\:\w+'))
for mtag in mtags:
dtags = soup.findAll(name=re.compile('^d\:\w+'))
tagDict = {}
for tag in dtags:
    tag = str(tag)
    tag = tag[3:]
    key = ""
    value = ""
    foundKey = False
    for char in tag:
        if not foundKey:
            if char  == '>':
                foundKey = True
            else:
                key = key + char
        else:
            if char == '<':
                break
            else:
                value = value + char
    #print(key, value)
    tagDict[key] = value
#simplejson.dump(tagDict, open('CSC.properties.1.json', 'w'))
