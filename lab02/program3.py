import os
finalstring = ""
directories = os.listdir("/bin")
maxlen = 0
for path in directories:
    if len(path) > maxlen:
        maxlen = len(path)
for i in range(maxlen + 1):
    lencount = 0
    for path in directories:
        if len(path) == i:
            lencount += 1
    finalstring = finalstring + str(i) + ", " + str(lencount) + "\n"
bincount = open("bincount.csv", "w")
bincount.write(finalstring)
bincount.close()
