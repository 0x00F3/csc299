datafile = open("data.csv", "r")
lines = datafile.readlines()
datafile.close()
sums = [0, 0, 0, 0]
for line in lines: 
    line = line.split()
    for i in range(len(line)):
        line[i] = int(line[i].replace(",", ""))
        sums[i] = sums[i] + line[i]
sums = str(sums)
sums = sums[1:len(sums) - 1]
sumsfile = open("sums.csv", "w")
sumsfile.write(sums)
sumsfile.close()
