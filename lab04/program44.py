import urllib
import re

pages = []
for i in range(11): 
    url = "http://mdp.cdm.depaul.edu/csc299/default/accounts?page=" + str(i)
    pages.append(urllib.urlopen(url).read())

total = 0
for page in pages:
    amts = re.findall("\$[0-9]+\.[0-9]+", page)
    for amt in amts:
        amt = amt.replace("$", "")
        #print(amt)
        amt = float(amt)
        total = total + amt
myFile = open("amount.total.txt", "w")
myFile.write(str(total))
myFile.close()
