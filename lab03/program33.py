expenses = open("expenses.csv", "r")
lines = expenses.readlines()
#print(lines)

class User:
    def __init__(self, idNum, spent):
        self.idNum = idNum
        self.spent = spent
    def addSpent(self, num):
        self.spent = self.spent + num
    def getID(self):
        return idNum
    def __repr__(self):
        return "{0:0>7}".format(self.idNum) + ", $" + "{0:2f}".format(float(self.spent))
users = []
lines[0] = "0,0"
for line in lines:
    line = line.split(",")
    line[1] = line[1].replace("$", "")
    line[1] = line[1].replace("\r\n", "")
    line[0] = int(line[0])
    line[1] = int(line[1])
    #print(line)
    amtAdded = False
    for user in users:
        if line[0] == user.idNum:
            user.addSpent(line[1])
            amtAdded = True
    if not amtAdded: users.append(User(line[0], line[1]))

maxID = 0
for user in users:
    if user.idNum > maxID:
        maxID = user.idNum

finalUsers = []
for i in range (maxID + 1):
    finalUsers.append(0)
    for user in users:
        if user.idNum == i:
            finalUsers[i] = user
            break

finalString = "USER_ID, TOTAL_EXPENSE\n"
for user in finalUsers:
    if user != 0:
        finalString = finalString + str(user) + "\n"
        
finalFile = open("totals.csv", "w")
finalFile.write(finalString)
finalFile.close()
