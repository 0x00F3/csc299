import mechanize
from BeautifulSoup import BeautifulSoup as Soup
import urllib
#Browser
browser = mechanize.Browser()
browser.set_handle_robots(False)
browser.addheaders = [("User-agent", "Firefox")]

#open files
urlCSV = raw_input("What file contains the URLs of the pages? ")
urlCSV = open(urlCSV, "r")
urls = urlCSV.readlines()[1:]
urlCSV.close()

keywordsCSV = raw_input("What file contains the keywords you want to use? ")
keywordsCSV = open(keywordsCSV, "r")
keywords = keywordsCSV.readlines()[1:]
keywordsCSV.close()
#print(keywords)

#get tags
pages = []
for url in urls:
    url = url.replace("\n", "")
    strhtml = urllib.urlopen(url).read()
    soup = Soup(strhtml)
    allTags = soup.findAll(True)
    pages+=allTags
    
#print("Completed: All tags found")

#get counts
counts = []
for keyword in keywords:
    keyword = keyword[:-1]
    count = 0
    for tag in allTags:
        if keyword in str(tag):
            count += 1
    counts.append(count)

#write to file
forfilestr = "KEYWORD, COUNT\n"
for i in range(len(keywords)):
    forfilestr += keywords[i][:-1] + ", " + str(counts[i]) + "\n"
#print(forfilestr)
wikimineoutput = open("wikimineoutput.csv", "w")
wikimineoutput.write(forfilestr)
wikimineoutput.close()
