from BeautifulSoup import BeautifulSoup as Soup
import urllib
import csv

url = "http://www.superherodb.com/characters/"
strhtml = urllib.urlopen(url).read()
output = open("names.csv", "wb")
writer = csv.writer(output)
writer.writerows([["NAME"]])

soup = Soup(strhtml)
litags = soup.findAll("li", {'class':"char-li"})
for litag in litags:
    name = litag.find("a").text
    writer.writerow([name])
output.close()
