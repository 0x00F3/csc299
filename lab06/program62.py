import mechanize
from BeautifulSoup import BeautifulSoup as Soup
import urllib
import re
import csv

def analyze(url):

    #Get tags
    strhtml = urllib.urlopen(url).read()
    soup = Soup(strhtml)
    name = soup.find("h1").text
    divtags = soup.findAll("div", {"class": re.compile("gridbarvalue.+")})
    divtags = divtags[:6]
    
    #create CSV
    output = open("superman.csv", "wb")
    writer = csv.writer(output)
    writer.writerows([["NAME", "INTELLIGENCE", "STRENGTH", "SPEED", "DURABILITY", "POWER", "COMBAT"]])
    stats = [name]
    for divtag in divtags:
        stats.append(divtag.text)
    writer.writerows([stats])
    output.close()

analyze("http://www.superherodb.com/superman/10-791/")

