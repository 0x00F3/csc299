import mechanize
from BeautifulSoup import BeautifulSoup as Soup
import urllib
import re
import csv

def analyze(url):

    #Get tags
    strhtml = urllib.urlopen(url).read()
    soup = Soup(strhtml)
    name = soup.find("h1").text
    divtags = soup.findAll("div", {"class": re.compile("gridbarvalue.+")})
    divtags = divtags[:6]
    
    #create CSV
    output = open("superman.csv", "wb")
    writer = csv.writer(output)
    writer.writerows([["NAME", "INTELLIGENCE", "STRENGTH", "SPEED", "DURABILITY", "POWER", "COMBAT"]])
    stats = [name]
    for divtag in divtags:
        stats.append(divtag.text)
    writer.writerows([stats])
    output.close()

#analyze("http://www.superherodb.com/superman/10-791/")
url = "http://www.superherodb.com/characters"
htmlstr = urllib.urlopen(url).read()
soup = Soup(htmlstr)
litags = soup.findAll("li", {"class" : "char-li"})
#print(litags)
rows = [["NAME", "INTELLIGENCE", "STRENGTH", "SPEED", "DURABILITY", "POWER", "COMBAT"]]
for litag in litags:
    href = litag.find('a').get('href')
    charurl = url + href
    heropage = urllib.urlopen(charurl).read()
    herosoup = Soup(heropage)
    name = herosoup.find("h1").text
    if name[0] == "B":
        break
    name = [name]
    divtags = herosoup.findAll("div", {"class" : re.compile("gridbarvalue.+")})
    divtags = divtags[:6]
    stats = []
    empties = ["", "", "", "", "", ""]
    for divtag in divtags:
        stats.append(divtag.text)
        
    if len(stats) == 6:
        row = [name + stats]
    else:
        row = [name + empties]
    print(row)
    rows = rows + row
output = open("superheros.csv", "wb")
writer = csv.writer(output)
writer.writerows(rows)
